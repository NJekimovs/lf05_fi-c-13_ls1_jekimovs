
public class AB2_Aufgabe2 {

	public static void main(String[] args) {
		// Aufgabe 2 
		
		//Erste Zeile
		System.out.printf("%-5s", "0!");
		System.out.printf("=");
		System.out.printf("%-19s", " ");
		System.out.printf("=");
		System.out.printf("%5s", "1\n");
		//Zweite Zeile
		System.out.printf("%-5s", "1!");
		System.out.printf("=");
		System.out.printf("%2s", "1");
		System.out.printf("%-17s", " ");
		System.out.printf("=");
		System.out.printf("%5s", "1\n");
		//Dritte Zeile
		System.out.printf("%-5s", "2!");
		System.out.printf("=");
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%-13s", " ");
		System.out.printf("=");
		System.out.printf("%5s", "2\n");
		//Vierte Zeile
		System.out.printf("%-5s", "3!");
		System.out.printf("=");
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "3");
		System.out.printf("%-9s", " ");
		System.out.printf("=");
		System.out.printf("%5s", "6\n");
		//F�nfte Zeile
		System.out.printf("%-5s", "4!");
		System.out.printf("=");
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "3");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "4");
		System.out.printf("%-5s", " ");
		System.out.printf("=");
		System.out.printf("%5s", "24\n");
		//Sechste Zeile
		System.out.printf("%-5s", "5!");
		System.out.printf("=");
		System.out.printf("%2s", "1");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "2");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "3");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "4");
		System.out.printf("%2s", "*");
		System.out.printf("%2s", "5");
		System.out.printf(" ");
		System.out.printf("=");
		System.out.printf("%4s", "120");
		//Ende
	}

}
