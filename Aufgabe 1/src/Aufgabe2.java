
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Mit Leerzeichen
		System.out.print("      *\n");
		System.out.print("     ***\n");
		System.out.print("    *****\n");
		System.out.print("   *******\n");
		System.out.print("  *********\n");
		System.out.print(" ***********\n");
		System.out.print("*************\n");
		System.out.print("     ***\n");
		System.out.print("     ***\n");

		//Eine Zeile da zwischen
		
		System.out.print(" \n");
		
		//Ohne Leerzeichen
		
		System.out.printf("%8s", "*\n");
		System.out.printf("%9s", "***\n");
		System.out.printf("%10s", "*****\n");
		System.out.printf("%11s", "*******\n");
		System.out.printf("%12s", "*********\n");
		System.out.printf("%13s", "***********\n");
		System.out.printf("%14s", "*************\n");
		System.out.printf("%9s", "***\n");
		System.out.printf("%9s", "***\n");
		
		
	}

		
	}

