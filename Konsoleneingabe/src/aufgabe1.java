import java.util.Scanner;

public class aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Neues Scanner-Objekt myScanner wird erstellt 
		 Scanner myScanner = new Scanner(System.in); 
		 
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
		 
		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt(); 
		 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 
		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt(); 
		 
		 // Die Addition der Variablen zahl1 und zahl2 
		 // wird der Variable ergebnis zugewiesen.
		 int addition = zahl1 + zahl2;
		 int deduction = zahl1 - zahl2;
		 int multiplication = zahl1 * zahl2;
		 int division = zahl1 / zahl2;
		 
		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + addition);
		 
		 System.out.print("\n\n\nErgebnis der Deduction lautet: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + deduction); 
		 
		 System.out.print("\n\n\nErgebnis der Multiplication lautet: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + multiplication); 
		 
		 System.out.print("\n\n\nErgebnis der Division lautet: ");
		 System.out.print(zahl1 + " / " + zahl2 + " = " + division); 
		 
		 myScanner.close();
		

	}

}
