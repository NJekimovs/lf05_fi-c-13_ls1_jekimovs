﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main (String[] args) {
		
		double gesamtepreis;
		double eingezahlterGesamtbetrag;
		for (int endlos = 0; endlos < 99999; endlos++) {
		gesamtepreis = fahrkartenbestellungErfassen();
		
		//Geldeinwurf
		eingezahlterGesamtbetrag = fahrkartenBezahlen(gesamtepreis);
		
		//Fahrscheinausgabe
		fahrkartenAusgeben();
		
		//Rückgeldberechnung und -Ausgabe
		rueckgeldAusgeben(eingezahlterGesamtbetrag, gesamtepreis);
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                			"vor Fahrtantritt entwerten zu lassen!\n"+
                			"Wir wünschen Ihnen eine gute Fahrt.");
	}
	}
	public static double fahrkartenbestellungErfassen() {
	
		double gesamtepreis;
		byte wahl;
		byte anzahlderTickets;
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println ("\nFahrkartenbestellvorgang:");
		for (int bestellvorgang = 0; bestellvorgang < 25; bestellvorgang++) {
			
			System.out.print("=");
			loading(250);
		}
		System.out.println("\n\n");
		
		double [] preise = { 2.90 , 3.30 , 3.60 , 1.90 , 8.60 , 9.00 , 9.60 , 23.50 , 24.30 , 24.90 ,  };
		String [] bezeichnung = {"Einzelfahrschein Berlin AB", 
								 "Einzelfahrschein BC",
								 "Einzelfahrschein ABC",
								 "Kurzstrecke",
								 "Tageskarte Berlin AB",
								 "Tageskarte Berlin BC",
								 "Tageskarte Berlin ABC",
								 "Kleingruppen-Tageskarte Berlin AB",
								 "Kleingruppen-Tageskarte Berlin BC",
								 "Kleingruppen-Tageskarte Berlin ABC",
								 };
		
		for (int i = 0; i < bezeichnung.length; i++) {
			System.out.printf ("  %-7d %-40s %.2f %s\n", (i+1) ,bezeichnung[i],preise [i], "Euro");
		}
		
		System.out.print ("\nIhre Wahl: ");
		wahl = myScanner.nextByte();
		
		while(wahl < 1 || wahl > bezeichnung.length) {
			System.out.println (">>Falsche Eingabe<<");
			System.out.print("Ihre Wahl: ");
			wahl = myScanner.nextByte();
		}

		
		System.out.println("Anzahl der Tickets: ");
		anzahlderTickets = myScanner.nextByte();
		
		while(anzahlderTickets < 1 || anzahlderTickets > 10) {
			System.out.println (">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus <<");
			System.out.println ("Anzahl der Tickets: ");
			anzahlderTickets = myScanner.nextByte();
		}
		
		gesamtepreis = preise[wahl-1] * anzahlderTickets;
		return gesamtepreis;
	}
	public static double fahrkartenBezahlen(double gesamtepreis) {
		Scanner myScanner = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		
		// Geldeinwurf
	    // -----------
	    eingezahlterGesamtbetrag = 0.0;
	    while(eingezahlterGesamtbetrag < gesamtepreis) {
	    	   System.out.printf("Noch zu zahlen: %.2f %s", gesamtepreis - eingezahlterGesamtbetrag, "€\n" );
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = myScanner.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       	}
		return eingezahlterGesamtbetrag;
	}
	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
	    // -----------------
	    System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          loading(250);
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldAusgeben (double eingezahlterGesamtbetrag, double nochzuZahlen) {
		
		// Rückgeldberechnung und -Ausgabe
	    // -------------------------------
		double rückgabebetrag;
	    rückgabebetrag = eingezahlterGesamtbetrag - nochzuZahlen;
	    if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro " , rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       	}
		}
		public static void loading (int milisekunde) {
			try {
				Thread.sleep(milisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
}