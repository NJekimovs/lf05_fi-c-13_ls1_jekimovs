import java.util.Scanner;
public class Aufgabe_8_Quadrat {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner (System.in);
		
		System.out.println ("Bitte geben Sie die L�nge des Quadrats ein: ");
		int laenge = scan.nextInt() -1;
		
		System.out.print ("");
		
		for (int x = 0; x <= laenge; x++) {
			for (int y = 0; y <= laenge; y++) {
				
				if (x == 0 || y == 0 || x == laenge || y == laenge) {
						System.out.printf("%2s", "*");
				}
				else {
						System.out.print("  ");
				}
			}
			System.out.print("\n");
		}
		scan.close();

	}

}
