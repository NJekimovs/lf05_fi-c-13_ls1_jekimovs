
public class Aufgabe_5_Einmaleins {

	public static void main(String[] args) {
		int zeile;
		int spalte;
		
		for (zeile = 1; zeile <=10; zeile++) {
			for (spalte = 1; spalte <=10; spalte++) {
				System.out.printf("%5d", spalte * zeile);
			}
			System.out.print ("\n");
		}

	}

}
