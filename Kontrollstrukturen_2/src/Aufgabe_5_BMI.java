import java.util.Scanner;
public class Aufgabe_5_BMI {

	public static void main(String[] args) {
		
	Scanner scan = new Scanner (System.in);
	
	System.out.println("Herzlich Wilkommen beim BMI Rechner!");
	System.out.println("Bitte geben Sie Ihre Körpergröße in Centimeter an");
	int groesse = scan.nextInt();
	
	System.out.println("Bitte geben Sie jetzt Ihr Gewicht in kg an");
	int gewicht = scan.nextInt();
	
	System.out.println ("Als letzes geben Sie bitte Ihr Geschlecht an [m/w]");
	char geschlecht = scan.next().charAt(0);
	
	double bmi_groesse = groesse/100.0;
	double bmi = gewicht/(bmi_groesse*bmi_groesse);
	scan.close();
	
	if (geschlecht == 'W' || geschlecht == 'w') {
		if (bmi < 19) {
			System.out.printf ("Sie haben Untergewicht %.2f ", bmi);
		}
		else if (bmi>=19 && bmi<=24) {
			System.out.printf ("Sie haben Normalgewicht %.2f",  bmi);
		}
		else {
			System.out.printf("Sie haben Übergewicht %.2f",  bmi);
		}
	}
	else if (geschlecht == 'M' || geschlecht == 'm') {
		if (bmi < 20) {
			System.out.printf("Sie haben Untergewicht %.2f ", bmi);
		}
		else if (bmi>=20 && bmi<=25) {
			System.out.printf("Sie haben Normalgewicht %.2f ", bmi);
		}
		else {
			System.out.printf ("Sie haben Übergewicht %.2f " , bmi);
		}
	}
	else {
		System.out.println ("Das ist kein Geschlecht");
	}
	}

}
