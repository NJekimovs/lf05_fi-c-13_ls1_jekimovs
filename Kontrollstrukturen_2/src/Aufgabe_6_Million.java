import java.util.Scanner;
public class Aufgabe_6_Million {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner (System.in);
		double kapital, zinssatz;
		char eingabe = 'j';
		
		while (eingabe == 'j') {
			
			int jahr = 1;
			
			System.out.println ("Herzlich Willkomen!");
			System.out.println ("Bitte geben Sie an, wie viel Geld Sie anlgen m�chten: ");
			kapital = scan.nextDouble();
			
			System.out.println ("Bitte geben Sie den gew�nschten Zinssatz an");
			zinssatz = scan.nextDouble();
			zinssatz = zinssatz / 100 + 1;
			
			while (kapital < 1000000) {
				kapital = kapital * zinssatz;
				jahr++;
			}
			
			System.out.println ("Nach " + jahr + " Jahren werden Sie ein Million�r!");
			
			System.out.println ("M�chten Sie einen neuen Durchlauf starten? [j/n]");
			eingabe = scan.next().charAt(0);
		}
		if (eingabe == 'n') {
			System.out.println ("Vielen Dank f�r die Nutzung unseres Rechners!");
		}
		scan.close();
	}

}
