import java.util.Scanner;
public class Aufgabe_5_Ohmsches_Gesetz {

	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		
		System.out.println ("Herzlich Willkomen beim Ohmsches Gesetz Taschenrechner!");
		System.out.println ("Was m�chten Sie berechen?");
		System.out.println ("(R) f�r Wiederstand");
		System.out.println ("(U) f�r Spannung");
		System.out.println ("(I) f�r Stromst�rke");
		char auswahl = scan.next().charAt(0);
		
		
		if (auswahl == 'R' || auswahl == 'r') {
			System.out.println ("Bitte geben Sie die Spannung in Volt ein:");
			double u = scan.nextDouble();
			System.out.println ("Bitte geben Sie die Stromst�rke in Amper ein: ");
			double i = scan.nextInt();
			double r = u / i;
			System.out.printf("R = %.2f Ohm", r);
		}
		else if (auswahl == 'U' || auswahl == 'u') {
			System.out.println ("Bitte geben Sie die Stromst�rke in Amper ein: ");
			double i = scan.nextInt();
			System.out.println ("Bitte geben Sie den Wiederstand in Ohm ein: ");
			double r = scan.nextDouble();
			double u = i * r;
			System.out.printf ("U = %.2f Volt", u);
		}
		else if (auswahl == 'I' || auswahl == 'i') {
			System.out.println ("Bitte geben Sie die Spannung in Volt ein:");
			double u = scan.nextDouble();
			System.out.println ("Bitte geben Sie den Wiederstand in Ohm ein: ");
			double r = scan.nextDouble();
			double i = u / r;
			System.out.printf("I = %.2f Ampere", i);
		}
		else {
			System.out.println ("Sie haben ein falsches Zeichnen eingegeben!");
		}
		scan.close();
	}

}
