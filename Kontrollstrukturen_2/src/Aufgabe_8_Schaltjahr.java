import java.util.Scanner;
public class Aufgabe_8_Schaltjahr {

	public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in);
	
	System.out.println ("Herzlich Willkomen beim Schaltjahr Rechner!");
	System.out.println ("Geben Sie den Jahreszahl: ");
	int jahr = scan.nextInt();
	
	scan.close();
	
	if (jahr % 4 == 0) {
		if (jahr > 1582) {
			if (jahr % 100 == 0) {
				if (jahr % 400 == 0) {
					System.out.println ("Es handelt sich um ein Schaltjahr!");
				}
				else {
					System.out.println ("Es handelt sich um kein Schaltjahr!");
				}
			}
			else {
				System.out.println ("Es handelt sich um ein Schaltjahr!");
			}
			
		}
	}
	else {
		System.out.println ("Es handelt sich um kein Schaltjahr!");
	}
	}
}
