import java.util.Scanner;
public class Aufgabe_4_Taschenrechner {

	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		
		double ergebnis;
		
		System.out.println ("Herzlich Willkommen beim Taschenrechner v2.0!");
		System.out.println ("Dieser Taschenrechner kann addieren, multiplizieren, dividieren und subtrahieren");
		
		//Zahl eingabe
		System.out.println ("Geben Sie den ersten Zahl ein: ");
		double zahl1 = scan.nextDouble();
		System.out.println ("Geben Sie den zweiten Zahl ein: ");
		double zahl2 = scan.nextDouble();
		
		//Rechnung Methode Auswahl
		System.out.println("Was soll berechnet werden? ");
		System.out.println("Bitte geben Sie einer der folgenden Zeichen ein:");
		System.out.println("(+) f�r Addieren");
		System.out.println("(-) f�r Subtrahieren");
		System.out.println("(*) f�r Multiplizieren");
		System.out.println("(/) f�r Dividieren");
		char auswahl = scan.next().charAt(0);
		
		scan.close();
		
		if (auswahl == '+') {
			ergebnis = zahl1 + zahl2;
			System.out.println (zahl1 + " " + auswahl + " " + zahl2 + " = " + ergebnis);
		}
		else if (auswahl == '-') {
			ergebnis = zahl1 - zahl2;
			System.out.println (zahl1 + " " + auswahl + " " + zahl2 + " = " + ergebnis);
		}
		else if (auswahl == '*') {
			ergebnis = zahl1 * zahl2;
			System.out.println (zahl1 + " " + auswahl + " " + zahl2 + " = " + ergebnis);
		}
		else if (auswahl == '/') {
			ergebnis = zahl1 / zahl2;
			System.out.println (zahl1 + " " + auswahl + " " + zahl2 + " = " + ergebnis);
		}
		else {
			System.out.println ("Sie haben ein Falsches Zeichen eingegeben!");
		}
		}
		
	}

