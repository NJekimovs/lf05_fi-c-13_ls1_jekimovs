import java.util.Scanner;
public class Aufgabe_8_Matrix {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner (System.in);
		
		System.out.println ("Herzlich Wilkommen!");
		System.out.println ("Bitte geben Sie ein Zahl zwischen 2 und 9 ein: ");
		int zahl = scan.nextInt();
		int zeichen = 0;
		
		System.out.print ("");
		
		while (zeichen < 100) {
				int ersteStelle = zeichen / 10;
				int zweiteStelle = zeichen % 10;
				
				if (zeichen % zahl == 0) {
					System.out.printf("%4s", "*");
				}
				else if (ersteStelle == zahl || zweiteStelle == zahl) {
					System.out.printf("%4s", "*");
				}
				else if (ersteStelle + zweiteStelle == zahl) {
					System.out.printf("%4s", "*");
				}
				else {
					System.out.printf("%4d", zeichen);
				}
				zeichen++;
				
				if (zeichen % 10 == 0 && zeichen !=0) {
					System.out.println("");
				}
		}
		scan.close();
	}

}
